export type RouteObject = {
  layout: string;
  path: string;
  icon: string;
  name: string;
};

export type Props = {
  location: {
    pathname: string;
  };
  logo: {
    innerLink?: string;
    outterLink?: string;
    imgSrc: string;
    imgAlt: string;
  };
  routes: Array<RouteObject>;
  bgColor: string;
};