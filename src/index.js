import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import { Provider } from 'react-redux';
import { store } from './store/store';

import 'assets/plugins/nucleo/css/nucleo.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'assets/scss/argon-dashboard-react.scss';

import AdminLayout from './layouts/Admin';
import AuthLayout from './layouts/Auth';

const rootElement = document.getElementById('root');

const renderApp = () =>
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
          <Route path="/" render={(props) => <AdminLayout {...props} />} />
          <Redirect from="/" to="/" />
        </Switch>
      </BrowserRouter>
    </Provider>,
    rootElement,
  );

renderApp();
