
import { logger } from "./middleware/logger";
import rootReducer from "./reducers";
import { Middleware, Reducer, Store } from 'redux'
import { Action, configureStore, ThunkAction, EnhancedStore } from "@reduxjs/toolkit";

const middleware = (getDefaultMiddleware: () => Middleware<any>[]) => getDefaultMiddleware().concat(logger)

export const store: EnhancedStore = configureStore({
	reducer: rootReducer,
	middleware,
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;
export type StoreEnhancer<S> = (next: GenericStoreEnhancerCreator) => StoreEnhancerStoreCreator<S>;
export type GenericStoreEnhancer = <S>(next: GenericStoreEnhancerCreator) => StoreEnhancerStoreCreator<S>;
export type StoreEnhancerStoreCreator<S> = (reducer: Reducer<S>, initialState: S) => Store<S>;
export type GenericStoreEnhancerCreator = <S>(reducer: Reducer<S>, initialState: S) => Store<S>;
