import { createAsyncThunk } from "@reduxjs/toolkit";
import { UserResponse, User } from "grpc/user_pb";
import { SelectAllUsers, SelectUser, CreateUser, UpdateUser, DeleteUser } from "../../api/user_api"

const FETCHALLUSERS = "user/fetchAllUsers"
const FETCHUSER = "user/fetchUser"

export type selectAllRequest = { org_id: string, page: number, size: number }
export type selectRequest = { user_id: string }

export const fetchAllUsers = createAsyncThunk<
  UserResponse.AsObject | null,
  selectAllRequest
>(
  FETCHALLUSERS,
  async ({ org_id, page, size }) => {
    const response: UserResponse.AsObject | null = await SelectAllUsers(org_id, page, size)
    return await response
  }
);

export const fetchUser = createAsyncThunk<
  UserResponse.AsObject,
  selectRequest
>(
  FETCHUSER,
  async ({ user_id }) => {
    const response: UserResponse.AsObject = await SelectUser(user_id)
    return await response
  }
);

export const createUser = createAsyncThunk<
  UserResponse.AsObject,
  User
>(
  FETCHUSER,
  async (user: User) => {
    const response: UserResponse.AsObject = await CreateUser(user)
    return await response
  }
);

export const updateUser = createAsyncThunk<
  UserResponse.AsObject,
  User
>(
  FETCHUSER,
  async (user: User) => {
    const response: UserResponse.AsObject = await UpdateUser(user)
    return await response
  }
);

export const deleteUser = createAsyncThunk<
  UserResponse.AsObject,
  selectRequest
>(
  FETCHUSER,
  async ({ user_id }) => {
    const response: UserResponse.AsObject = await DeleteUser(user_id)
    return await response
  }
);