import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiCall, APIResponse } from "../../api";

const LOGIN = "auth/login"
const REGISTER = "auth/register"
const FORGOTPASSWORD = "auth/forgotPassword"

export type NewUserForm = {
  name: string
  last_name: string
  company: string
}

export type Request = {
  email: string
  password?: string
  newUser?: NewUserForm
}

export const login = createAsyncThunk<
  APIResponse,
  Request
>(
  LOGIN,
  async (req: Request) => {
    const { email, password } = req
    const response: APIResponse = await apiCall('POST', '/api/auth/login', {}, { email, password })
    return await response
  }
);

export const register = createAsyncThunk<
  APIResponse,
  Request
>(
  REGISTER,
  async (req: Request) => {
    const { email, password, newUser } = req
    const response: APIResponse = await apiCall('POST', '/api/auth/register', {}, { email, password, newUser })
    return await response
  }
);

export const forgotPassword = createAsyncThunk<
  APIResponse,
  Request
>(
  FORGOTPASSWORD,
  async (req: Request) => {
    const { email } = req
    const response: APIResponse = await apiCall('GET', 'users', {}, { email })
    return await response
  }
);