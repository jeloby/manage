import { combineReducers } from '@reduxjs/toolkit'

// import topicReducer from "./topics";
import userSlice from "./reducers/user_reducer";
import imagesReducer from "./reducers/images";

const rootReducer = combineReducers({
	user: userSlice,
	images: imagesReducer
});

export default rootReducer;
