import { createSlice, current } from '@reduxjs/toolkit';
import { User, UserRequest, UserResponse } from '../../grpc/user_pb';
import { Pagination } from 'grpc/common_pb';
import { APIResponse } from 'api';
import type { RootState } from 'store/store';
import { fetchAllUsers, fetchUser } from 'store/actions/user_actions';
import { login, register, forgotPassword } from 'store/actions/auth_actions';

import { ProtobufTypeDefinition } from '@grpc/grpc-js';


type UserState = {
    users?: User.AsObject[];
    pagination?: Pagination.AsObject;
    isLoading: boolean;
};

const initialState: UserState = {
    isLoading: false,
    pagination: {
        current: 0,
        size: 10,
        total: 0
    }
};

export const userSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllUsers.pending, (state) => loading(state))
            .addCase(fetchAllUsers.fulfilled, (state, action) => updateUsers(state, action.payload))

            .addCase(fetchUser.pending, (state) => loading(state))
            .addCase(fetchUser.fulfilled, (state, action) => updateUsers(state, action.payload));
    },
});

const loading = (state: UserState) => {
    state.isLoading = true;
};

const updateUsers = (state: UserState, payload: UserResponse.AsObject | null) => {
    if (payload) {
        state.isLoading = false;
        if (payload.usersList.length > 0) {
            if (state.users) {
                state.users.push(...payload.usersList);
            } else {
                state.users = payload.usersList;
            }
        }
        state.pagination = payload.pagination
    }
};

export const selectUser = (state: RootState) => state.user;

export default userSlice.reducer;
