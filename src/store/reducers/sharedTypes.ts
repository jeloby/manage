export type { Pagination } from "../../api";

// import { Pagination as Pager} from "../../api";

// export type Pagination = Pager

export const newPagination = (page?: number, size?: number, total?: number) => {
  return {
    page: (page || 0),
    size: (size || 0),
    total: (total || 0),
  }
}