/**
 * @fileoverview gRPC-Web generated client stub for user
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as user_pb from './user_pb';


export class UserServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoSelectAll = new grpcWeb.MethodDescriptor(
    '/user.UserService/SelectAll',
    grpcWeb.MethodType.UNARY,
    user_pb.UserRequest,
    user_pb.UserResponse,
    (request: user_pb.UserRequest) => {
      return request.serializeBinary();
    },
    user_pb.UserResponse.deserializeBinary
  );

  selectAll(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_pb.UserResponse>;

  selectAll(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void): grpcWeb.ClientReadableStream<user_pb.UserResponse>;

  selectAll(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user.UserService/SelectAll',
        request,
        metadata || {},
        this.methodInfoSelectAll,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user.UserService/SelectAll',
    request,
    metadata || {},
    this.methodInfoSelectAll);
  }

  methodInfoSelect = new grpcWeb.MethodDescriptor(
    '/user.UserService/Select',
    grpcWeb.MethodType.UNARY,
    user_pb.UserRequest,
    user_pb.UserResponse,
    (request: user_pb.UserRequest) => {
      return request.serializeBinary();
    },
    user_pb.UserResponse.deserializeBinary
  );

  select(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_pb.UserResponse>;

  select(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void): grpcWeb.ClientReadableStream<user_pb.UserResponse>;

  select(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user.UserService/Select',
        request,
        metadata || {},
        this.methodInfoSelect,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user.UserService/Select',
    request,
    metadata || {},
    this.methodInfoSelect);
  }

  methodInfoCreate = new grpcWeb.MethodDescriptor(
    '/user.UserService/Create',
    grpcWeb.MethodType.UNARY,
    user_pb.UserRequest,
    user_pb.UserResponse,
    (request: user_pb.UserRequest) => {
      return request.serializeBinary();
    },
    user_pb.UserResponse.deserializeBinary
  );

  create(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_pb.UserResponse>;

  create(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void): grpcWeb.ClientReadableStream<user_pb.UserResponse>;

  create(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user.UserService/Create',
        request,
        metadata || {},
        this.methodInfoCreate,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user.UserService/Create',
    request,
    metadata || {},
    this.methodInfoCreate);
  }

  methodInfoUpdate = new grpcWeb.MethodDescriptor(
    '/user.UserService/Update',
    grpcWeb.MethodType.UNARY,
    user_pb.UserRequest,
    user_pb.UserResponse,
    (request: user_pb.UserRequest) => {
      return request.serializeBinary();
    },
    user_pb.UserResponse.deserializeBinary
  );

  update(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_pb.UserResponse>;

  update(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void): grpcWeb.ClientReadableStream<user_pb.UserResponse>;

  update(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user.UserService/Update',
        request,
        metadata || {},
        this.methodInfoUpdate,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user.UserService/Update',
    request,
    metadata || {},
    this.methodInfoUpdate);
  }

  methodInfoDelete = new grpcWeb.MethodDescriptor(
    '/user.UserService/Delete',
    grpcWeb.MethodType.UNARY,
    user_pb.UserRequest,
    user_pb.UserResponse,
    (request: user_pb.UserRequest) => {
      return request.serializeBinary();
    },
    user_pb.UserResponse.deserializeBinary
  );

  delete(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_pb.UserResponse>;

  delete(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void): grpcWeb.ClientReadableStream<user_pb.UserResponse>;

  delete(
    request: user_pb.UserRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_pb.UserResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user.UserService/Delete',
        request,
        metadata || {},
        this.methodInfoDelete,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user.UserService/Delete',
    request,
    metadata || {},
    this.methodInfoDelete);
  }

}

