import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class UserMeta extends jspb.Message {
  getId(): string;
  setId(value: string): UserMeta;

  getUserid(): string;
  setUserid(value: string): UserMeta;

  getPayload(): string;
  setPayload(value: string): UserMeta;

  getNotesid(): string;
  setNotesid(value: string): UserMeta;

  getUpdatedat(): number;
  setUpdatedat(value: number): UserMeta;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserMeta.AsObject;
  static toObject(includeInstance: boolean, msg: UserMeta): UserMeta.AsObject;
  static serializeBinaryToWriter(message: UserMeta, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserMeta;
  static deserializeBinaryFromReader(message: UserMeta, reader: jspb.BinaryReader): UserMeta;
}

export namespace UserMeta {
  export type AsObject = {
    id: string,
    userid: string,
    payload: string,
    notesid: string,
    updatedat: number,
  }
}

export class UserMetaRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): UserMetaRequest;
  hasHdr(): boolean;
  clearHdr(): UserMetaRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): UserMetaRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): UserMetaRequest;
  hasFilter(): boolean;
  clearFilter(): UserMetaRequest;

  getUsermeta(): UserMeta | undefined;
  setUsermeta(value?: UserMeta): UserMetaRequest;
  hasUsermeta(): boolean;
  clearUsermeta(): UserMetaRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserMetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UserMetaRequest): UserMetaRequest.AsObject;
  static serializeBinaryToWriter(message: UserMetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserMetaRequest;
  static deserializeBinaryFromReader(message: UserMetaRequest, reader: jspb.BinaryReader): UserMetaRequest;
}

export namespace UserMetaRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    usermeta?: UserMeta.AsObject,
  }
}

export class UserMetaResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): UserMetaResponse;
  hasHdr(): boolean;
  clearHdr(): UserMetaResponse;

  getError(): string;
  setError(value: string): UserMetaResponse;

  getUsermeta(): UserMeta | undefined;
  setUsermeta(value?: UserMeta): UserMetaResponse;
  hasUsermeta(): boolean;
  clearUsermeta(): UserMetaResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserMetaResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UserMetaResponse): UserMetaResponse.AsObject;
  static serializeBinaryToWriter(message: UserMetaResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserMetaResponse;
  static deserializeBinaryFromReader(message: UserMetaResponse, reader: jspb.BinaryReader): UserMetaResponse;
}

export namespace UserMetaResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    usermeta?: UserMeta.AsObject,
  }
}

