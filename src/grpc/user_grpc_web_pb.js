/**
 * @fileoverview gRPC-Web generated client stub for user
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var common_pb = require('./common_pb.js')
const proto = {};
proto.user = require('./user_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.user.UserServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.user.UserServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserRequest,
 *   !proto.user.UserResponse>}
 */
const methodDescriptor_UserService_SelectAll = new grpc.web.MethodDescriptor(
  '/user.UserService/SelectAll',
  grpc.web.MethodType.UNARY,
  proto.user.UserRequest,
  proto.user.UserResponse,
  /**
   * @param {!proto.user.UserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.UserResponse.deserializeBinary
);


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user.UserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.UserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserServiceClient.prototype.selectAll =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserService/SelectAll',
      request,
      metadata || {},
      methodDescriptor_UserService_SelectAll,
      callback);
};


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.UserResponse>}
 *     Promise that resolves to the response
 */
proto.user.UserServicePromiseClient.prototype.selectAll =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserService/SelectAll',
      request,
      metadata || {},
      methodDescriptor_UserService_SelectAll);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserRequest,
 *   !proto.user.UserResponse>}
 */
const methodDescriptor_UserService_Select = new grpc.web.MethodDescriptor(
  '/user.UserService/Select',
  grpc.web.MethodType.UNARY,
  proto.user.UserRequest,
  proto.user.UserResponse,
  /**
   * @param {!proto.user.UserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.UserResponse.deserializeBinary
);


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user.UserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.UserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserServiceClient.prototype.select =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserService/Select',
      request,
      metadata || {},
      methodDescriptor_UserService_Select,
      callback);
};


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.UserResponse>}
 *     Promise that resolves to the response
 */
proto.user.UserServicePromiseClient.prototype.select =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserService/Select',
      request,
      metadata || {},
      methodDescriptor_UserService_Select);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserRequest,
 *   !proto.user.UserResponse>}
 */
const methodDescriptor_UserService_Create = new grpc.web.MethodDescriptor(
  '/user.UserService/Create',
  grpc.web.MethodType.UNARY,
  proto.user.UserRequest,
  proto.user.UserResponse,
  /**
   * @param {!proto.user.UserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.UserResponse.deserializeBinary
);


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user.UserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.UserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserServiceClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserService/Create',
      request,
      metadata || {},
      methodDescriptor_UserService_Create,
      callback);
};


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.UserResponse>}
 *     Promise that resolves to the response
 */
proto.user.UserServicePromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserService/Create',
      request,
      metadata || {},
      methodDescriptor_UserService_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserRequest,
 *   !proto.user.UserResponse>}
 */
const methodDescriptor_UserService_Update = new grpc.web.MethodDescriptor(
  '/user.UserService/Update',
  grpc.web.MethodType.UNARY,
  proto.user.UserRequest,
  proto.user.UserResponse,
  /**
   * @param {!proto.user.UserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.UserResponse.deserializeBinary
);


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user.UserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.UserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserServiceClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserService/Update',
      request,
      metadata || {},
      methodDescriptor_UserService_Update,
      callback);
};


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.UserResponse>}
 *     Promise that resolves to the response
 */
proto.user.UserServicePromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserService/Update',
      request,
      metadata || {},
      methodDescriptor_UserService_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user.UserRequest,
 *   !proto.user.UserResponse>}
 */
const methodDescriptor_UserService_Delete = new grpc.web.MethodDescriptor(
  '/user.UserService/Delete',
  grpc.web.MethodType.UNARY,
  proto.user.UserRequest,
  proto.user.UserResponse,
  /**
   * @param {!proto.user.UserRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user.UserResponse.deserializeBinary
);


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user.UserResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user.UserResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user.UserServiceClient.prototype.delete =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user.UserService/Delete',
      request,
      metadata || {},
      methodDescriptor_UserService_Delete,
      callback);
};


/**
 * @param {!proto.user.UserRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user.UserResponse>}
 *     Promise that resolves to the response
 */
proto.user.UserServicePromiseClient.prototype.delete =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user.UserService/Delete',
      request,
      metadata || {},
      methodDescriptor_UserService_Delete);
};


module.exports = proto.user;

