import * as jspb from 'google-protobuf'



export class CachedSession extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): CachedSession;

  getUseruuid(): string;
  setUseruuid(value: string): CachedSession;

  getOrguuid(): string;
  setOrguuid(value: string): CachedSession;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CachedSession.AsObject;
  static toObject(includeInstance: boolean, msg: CachedSession): CachedSession.AsObject;
  static serializeBinaryToWriter(message: CachedSession, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CachedSession;
  static deserializeBinaryFromReader(message: CachedSession, reader: jspb.BinaryReader): CachedSession;
}

export namespace CachedSession {
  export type AsObject = {
    email: string,
    useruuid: string,
    orguuid: string,
  }
}

export class Login extends jspb.Message {
  getEmail(): string;
  setEmail(value: string): Login;

  getPassword(): string;
  setPassword(value: string): Login;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Login.AsObject;
  static toObject(includeInstance: boolean, msg: Login): Login.AsObject;
  static serializeBinaryToWriter(message: Login, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Login;
  static deserializeBinaryFromReader(message: Login, reader: jspb.BinaryReader): Login;
}

export namespace Login {
  export type AsObject = {
    email: string,
    password: string,
  }
}

export class Validate extends jspb.Message {
  getUserkey(): string;
  setUserkey(value: string): Validate;

  getCompanykey(): string;
  setCompanykey(value: string): Validate;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Validate.AsObject;
  static toObject(includeInstance: boolean, msg: Validate): Validate.AsObject;
  static serializeBinaryToWriter(message: Validate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Validate;
  static deserializeBinaryFromReader(message: Validate, reader: jspb.BinaryReader): Validate;
}

export namespace Validate {
  export type AsObject = {
    userkey: string,
    companykey: string,
  }
}

