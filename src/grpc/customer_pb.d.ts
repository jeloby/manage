import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class Customer extends jspb.Message {
  getId(): string;
  setId(value: string): Customer;

  getOrgid(): string;
  setOrgid(value: string): Customer;

  getName(): string;
  setName(value: string): Customer;

  getPayload(): string;
  setPayload(value: string): Customer;

  getNotesid(): string;
  setNotesid(value: string): Customer;

  getCreatedby(): string;
  setCreatedby(value: string): Customer;

  getCreatedat(): number;
  setCreatedat(value: number): Customer;

  getUpdatedat(): number;
  setUpdatedat(value: number): Customer;

  getDeletedat(): number;
  setDeletedat(value: number): Customer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Customer.AsObject;
  static toObject(includeInstance: boolean, msg: Customer): Customer.AsObject;
  static serializeBinaryToWriter(message: Customer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Customer;
  static deserializeBinaryFromReader(message: Customer, reader: jspb.BinaryReader): Customer;
}

export namespace Customer {
  export type AsObject = {
    id: string,
    orgid: string,
    name: string,
    payload: string,
    notesid: string,
    createdby: string,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }
}

export class CustomerRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): CustomerRequest;
  hasHdr(): boolean;
  clearHdr(): CustomerRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): CustomerRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerRequest;
  hasPagination(): boolean;
  clearPagination(): CustomerRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): CustomerRequest;
  hasFilter(): boolean;
  clearFilter(): CustomerRequest;

  getCustomer(): Customer | undefined;
  setCustomer(value?: Customer): CustomerRequest;
  hasCustomer(): boolean;
  clearCustomer(): CustomerRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerRequest): CustomerRequest.AsObject;
  static serializeBinaryToWriter(message: CustomerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerRequest;
  static deserializeBinaryFromReader(message: CustomerRequest, reader: jspb.BinaryReader): CustomerRequest;
}

export namespace CustomerRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    customer?: Customer.AsObject,
  }
}

export class CustomersResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): CustomersResponse;
  hasHdr(): boolean;
  clearHdr(): CustomersResponse;

  getError(): string;
  setError(value: string): CustomersResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomersResponse;
  hasPagination(): boolean;
  clearPagination(): CustomersResponse;

  getCustomersList(): Array<Customer>;
  setCustomersList(value: Array<Customer>): CustomersResponse;
  clearCustomersList(): CustomersResponse;
  addCustomers(value?: Customer, index?: number): Customer;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomersResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CustomersResponse): CustomersResponse.AsObject;
  static serializeBinaryToWriter(message: CustomersResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomersResponse;
  static deserializeBinaryFromReader(message: CustomersResponse, reader: jspb.BinaryReader): CustomersResponse;
}

export namespace CustomersResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    customersList: Array<Customer.AsObject>,
  }
}

export class CustomerUser extends jspb.Message {
  getId(): string;
  setId(value: string): CustomerUser;

  getOrgid(): string;
  setOrgid(value: string): CustomerUser;

  getCustomerid(): string;
  setCustomerid(value: string): CustomerUser;

  getFirstName(): string;
  setFirstName(value: string): CustomerUser;

  getLastName(): string;
  setLastName(value: string): CustomerUser;

  getEmail(): string;
  setEmail(value: string): CustomerUser;

  getPhone(): string;
  setPhone(value: string): CustomerUser;

  getRole(): string;
  setRole(value: string): CustomerUser;

  getNotesId(): string;
  setNotesId(value: string): CustomerUser;

  getCreatedBy(): string;
  setCreatedBy(value: string): CustomerUser;

  getCreatedAt(): number;
  setCreatedAt(value: number): CustomerUser;

  getUpdatedAt(): number;
  setUpdatedAt(value: number): CustomerUser;

  getDeletedAt(): number;
  setDeletedAt(value: number): CustomerUser;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerUser.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerUser): CustomerUser.AsObject;
  static serializeBinaryToWriter(message: CustomerUser, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerUser;
  static deserializeBinaryFromReader(message: CustomerUser, reader: jspb.BinaryReader): CustomerUser;
}

export namespace CustomerUser {
  export type AsObject = {
    id: string,
    orgid: string,
    customerid: string,
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    role: string,
    notesId: string,
    createdBy: string,
    createdAt: number,
    updatedAt: number,
    deletedAt: number,
  }
}

export class CustomerUserRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): CustomerUserRequest;
  hasHdr(): boolean;
  clearHdr(): CustomerUserRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): CustomerUserRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): CustomerUserRequest;
  hasFilter(): boolean;
  clearFilter(): CustomerUserRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerUserRequest;
  hasPagination(): boolean;
  clearPagination(): CustomerUserRequest;

  getCustomeruser(): CustomerUser | undefined;
  setCustomeruser(value?: CustomerUser): CustomerUserRequest;
  hasCustomeruser(): boolean;
  clearCustomeruser(): CustomerUserRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerUserRequest): CustomerUserRequest.AsObject;
  static serializeBinaryToWriter(message: CustomerUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerUserRequest;
  static deserializeBinaryFromReader(message: CustomerUserRequest, reader: jspb.BinaryReader): CustomerUserRequest;
}

export namespace CustomerUserRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    pagination?: common_pb.Pagination.AsObject,
    customeruser?: CustomerUser.AsObject,
  }
}

export class CustomerUserResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): CustomerUserResponse;
  hasHdr(): boolean;
  clearHdr(): CustomerUserResponse;

  getError(): string;
  setError(value: string): CustomerUserResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerUserResponse;
  hasPagination(): boolean;
  clearPagination(): CustomerUserResponse;

  getCustomerusersList(): Array<CustomerUser>;
  setCustomerusersList(value: Array<CustomerUser>): CustomerUserResponse;
  clearCustomerusersList(): CustomerUserResponse;
  addCustomerusers(value?: CustomerUser, index?: number): CustomerUser;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerUserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerUserResponse): CustomerUserResponse.AsObject;
  static serializeBinaryToWriter(message: CustomerUserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerUserResponse;
  static deserializeBinaryFromReader(message: CustomerUserResponse, reader: jspb.BinaryReader): CustomerUserResponse;
}

export namespace CustomerUserResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    customerusersList: Array<CustomerUser.AsObject>,
  }
}

export class CustomerComment extends jspb.Message {
  getId(): string;
  setId(value: string): CustomerComment;

  getCustomerid(): string;
  setCustomerid(value: string): CustomerComment;

  getCustomeruser(): string;
  setCustomeruser(value: string): CustomerComment;

  getNotesId(): string;
  setNotesId(value: string): CustomerComment;

  getCreatedBy(): string;
  setCreatedBy(value: string): CustomerComment;

  getCreatedAt(): number;
  setCreatedAt(value: number): CustomerComment;

  getUpdatedAt(): number;
  setUpdatedAt(value: number): CustomerComment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerComment.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerComment): CustomerComment.AsObject;
  static serializeBinaryToWriter(message: CustomerComment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerComment;
  static deserializeBinaryFromReader(message: CustomerComment, reader: jspb.BinaryReader): CustomerComment;
}

export namespace CustomerComment {
  export type AsObject = {
    id: string,
    customerid: string,
    customeruser: string,
    notesId: string,
    createdBy: string,
    createdAt: number,
    updatedAt: number,
  }
}

export class CustomerCommentRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): CustomerCommentRequest;
  hasHdr(): boolean;
  clearHdr(): CustomerCommentRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): CustomerCommentRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): CustomerCommentRequest;
  hasFilter(): boolean;
  clearFilter(): CustomerCommentRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerCommentRequest;
  hasPagination(): boolean;
  clearPagination(): CustomerCommentRequest;

  getCustomercomment(): CustomerComment | undefined;
  setCustomercomment(value?: CustomerComment): CustomerCommentRequest;
  hasCustomercomment(): boolean;
  clearCustomercomment(): CustomerCommentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerCommentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerCommentRequest): CustomerCommentRequest.AsObject;
  static serializeBinaryToWriter(message: CustomerCommentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerCommentRequest;
  static deserializeBinaryFromReader(message: CustomerCommentRequest, reader: jspb.BinaryReader): CustomerCommentRequest;
}

export namespace CustomerCommentRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    pagination?: common_pb.Pagination.AsObject,
    customercomment?: CustomerComment.AsObject,
  }
}

export class CustomerCommentResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): CustomerCommentResponse;
  hasHdr(): boolean;
  clearHdr(): CustomerCommentResponse;

  getError(): string;
  setError(value: string): CustomerCommentResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerCommentResponse;
  hasPagination(): boolean;
  clearPagination(): CustomerCommentResponse;

  getCustomercommentsList(): Array<CustomerComment>;
  setCustomercommentsList(value: Array<CustomerComment>): CustomerCommentResponse;
  clearCustomercommentsList(): CustomerCommentResponse;
  addCustomercomments(value?: CustomerComment, index?: number): CustomerComment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerCommentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerCommentResponse): CustomerCommentResponse.AsObject;
  static serializeBinaryToWriter(message: CustomerCommentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerCommentResponse;
  static deserializeBinaryFromReader(message: CustomerCommentResponse, reader: jspb.BinaryReader): CustomerCommentResponse;
}

export namespace CustomerCommentResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    customercommentsList: Array<CustomerComment.AsObject>,
  }
}

export class CustomerAssignment extends jspb.Message {
  getId(): string;
  setId(value: string): CustomerAssignment;

  getCustomerid(): string;
  setCustomerid(value: string): CustomerAssignment;

  getUser(): string;
  setUser(value: string): CustomerAssignment;

  getComment(): string;
  setComment(value: string): CustomerAssignment;

  getCreatedBy(): string;
  setCreatedBy(value: string): CustomerAssignment;

  getCreatedAt(): number;
  setCreatedAt(value: number): CustomerAssignment;

  getUpdatedAt(): number;
  setUpdatedAt(value: number): CustomerAssignment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerAssignment.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerAssignment): CustomerAssignment.AsObject;
  static serializeBinaryToWriter(message: CustomerAssignment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerAssignment;
  static deserializeBinaryFromReader(message: CustomerAssignment, reader: jspb.BinaryReader): CustomerAssignment;
}

export namespace CustomerAssignment {
  export type AsObject = {
    id: string,
    customerid: string,
    user: string,
    comment: string,
    createdBy: string,
    createdAt: number,
    updatedAt: number,
  }
}

export class CustomerAssignmentRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): CustomerAssignmentRequest;
  hasHdr(): boolean;
  clearHdr(): CustomerAssignmentRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): CustomerAssignmentRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): CustomerAssignmentRequest;
  hasFilter(): boolean;
  clearFilter(): CustomerAssignmentRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerAssignmentRequest;
  hasPagination(): boolean;
  clearPagination(): CustomerAssignmentRequest;

  getCustomerassignment(): CustomerAssignment | undefined;
  setCustomerassignment(value?: CustomerAssignment): CustomerAssignmentRequest;
  hasCustomerassignment(): boolean;
  clearCustomerassignment(): CustomerAssignmentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerAssignmentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerAssignmentRequest): CustomerAssignmentRequest.AsObject;
  static serializeBinaryToWriter(message: CustomerAssignmentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerAssignmentRequest;
  static deserializeBinaryFromReader(message: CustomerAssignmentRequest, reader: jspb.BinaryReader): CustomerAssignmentRequest;
}

export namespace CustomerAssignmentRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    pagination?: common_pb.Pagination.AsObject,
    customerassignment?: CustomerAssignment.AsObject,
  }
}

export class CustomerAssignmentResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): CustomerAssignmentResponse;
  hasHdr(): boolean;
  clearHdr(): CustomerAssignmentResponse;

  getError(): string;
  setError(value: string): CustomerAssignmentResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): CustomerAssignmentResponse;
  hasPagination(): boolean;
  clearPagination(): CustomerAssignmentResponse;

  getCustomerassignmentsList(): Array<CustomerAssignment>;
  setCustomerassignmentsList(value: Array<CustomerAssignment>): CustomerAssignmentResponse;
  clearCustomerassignmentsList(): CustomerAssignmentResponse;
  addCustomerassignments(value?: CustomerAssignment, index?: number): CustomerAssignment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CustomerAssignmentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CustomerAssignmentResponse): CustomerAssignmentResponse.AsObject;
  static serializeBinaryToWriter(message: CustomerAssignmentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CustomerAssignmentResponse;
  static deserializeBinaryFromReader(message: CustomerAssignmentResponse, reader: jspb.BinaryReader): CustomerAssignmentResponse;
}

export namespace CustomerAssignmentResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    customerassignmentsList: Array<CustomerAssignment.AsObject>,
  }
}

