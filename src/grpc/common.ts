/**
 * Generated by the protoc-gen-ts.  DO NOT EDIT!
 * compiler version: 3.17.2
 * source: common.proto
 * git: https://github.com/thesayyn/protoc-gen-ts
 *  */
import * as pb_1 from "google-protobuf";
export namespace common {
    export enum Role {
        Admin = 0,
        Moderator = 1,
        ReadOnly = 2
    }
    export enum Type {
        Trail = 0,
        Basic = 1,
        Premium = 2
    }
    export enum Status {
        Active = 0,
        Expired = 2,
        Deactivated = 3,
        Blocked = 4
    }
    export enum ModelCRUD {
        Create = 0,
        Select = 1,
        SelectAll = 2,
        Update = 3,
        Delete = 4,
        Search = 5,
        Stale = 6,
        Enrol = 7,
        Enrolled = 8,
        Upsert = 9,
        Current = 10,
        Action = 11
    }
    export class Filter extends pb_1.Message {
        constructor(data?: any[] | {
            raw?: string;
        }) {
            super();
            pb_1.Message.initialize(this, Array.isArray(data) ? data : [], 0, -1, [], []);
            if (!Array.isArray(data) && typeof data == "object") {
                if ("raw" in data && data.raw != undefined) {
                    this.raw = data.raw;
                }
            }
        }
        get raw() {
            return pb_1.Message.getField(this, 1) as string;
        }
        set raw(value: string) {
            pb_1.Message.setField(this, 1, value);
        }
        static fromObject(data: {
            raw?: string;
        }) {
            const message = new Filter({});
            if (data.raw != null) {
                message.raw = data.raw;
            }
            return message;
        }
        toObject() {
            const data: {
                raw?: string;
            } = {};
            if (this.raw != null) {
                data.raw = this.raw;
            }
            return data;
        }
        serialize(): Uint8Array;
        serialize(w: pb_1.BinaryWriter): void;
        serialize(w?: pb_1.BinaryWriter): Uint8Array | void {
            const writer = w || new pb_1.BinaryWriter();
            if (typeof this.raw === "string" && this.raw.length)
                writer.writeString(1, this.raw);
            if (!w)
                return writer.getResultBuffer();
        }
        static deserialize(bytes: Uint8Array | pb_1.BinaryReader): Filter {
            const reader = bytes instanceof pb_1.BinaryReader ? bytes : new pb_1.BinaryReader(bytes), message = new Filter();
            while (reader.nextField()) {
                if (reader.isEndGroup())
                    break;
                switch (reader.getFieldNumber()) {
                    case 1:
                        message.raw = reader.readString();
                        break;
                    default: reader.skipField();
                }
            }
            return message;
        }
        serializeBinary(): Uint8Array {
            return this.serialize();
        }
        static deserializeBinary(bytes: Uint8Array): Filter {
            return Filter.deserialize(bytes);
        }
    }
    export class Pagination extends pb_1.Message {
        constructor(data?: any[] | {
            Current?: number;
            Size?: number;
            Total?: number;
        }) {
            super();
            pb_1.Message.initialize(this, Array.isArray(data) ? data : [], 0, -1, [], []);
            if (!Array.isArray(data) && typeof data == "object") {
                if ("Current" in data && data.Current != undefined) {
                    this.Current = data.Current;
                }
                if ("Size" in data && data.Size != undefined) {
                    this.Size = data.Size;
                }
                if ("Total" in data && data.Total != undefined) {
                    this.Total = data.Total;
                }
            }
        }
        get Current() {
            return pb_1.Message.getField(this, 1) as number;
        }
        set Current(value: number) {
            pb_1.Message.setField(this, 1, value);
        }
        get Size() {
            return pb_1.Message.getField(this, 2) as number;
        }
        set Size(value: number) {
            pb_1.Message.setField(this, 2, value);
        }
        get Total() {
            return pb_1.Message.getField(this, 3) as number;
        }
        set Total(value: number) {
            pb_1.Message.setField(this, 3, value);
        }
        static fromObject(data: {
            Current?: number;
            Size?: number;
            Total?: number;
        }) {
            const message = new Pagination({});
            if (data.Current != null) {
                message.Current = data.Current;
            }
            if (data.Size != null) {
                message.Size = data.Size;
            }
            if (data.Total != null) {
                message.Total = data.Total;
            }
            return message;
        }
        toObject() {
            const data: {
                Current?: number;
                Size?: number;
                Total?: number;
            } = {};
            if (this.Current != null) {
                data.Current = this.Current;
            }
            if (this.Size != null) {
                data.Size = this.Size;
            }
            if (this.Total != null) {
                data.Total = this.Total;
            }
            return data;
        }
        serialize(): Uint8Array;
        serialize(w: pb_1.BinaryWriter): void;
        serialize(w?: pb_1.BinaryWriter): Uint8Array | void {
            const writer = w || new pb_1.BinaryWriter();
            if (this.Current !== undefined)
                writer.writeInt32(1, this.Current);
            if (this.Size !== undefined)
                writer.writeInt32(2, this.Size);
            if (this.Total !== undefined)
                writer.writeInt32(3, this.Total);
            if (!w)
                return writer.getResultBuffer();
        }
        static deserialize(bytes: Uint8Array | pb_1.BinaryReader): Pagination {
            const reader = bytes instanceof pb_1.BinaryReader ? bytes : new pb_1.BinaryReader(bytes), message = new Pagination();
            while (reader.nextField()) {
                if (reader.isEndGroup())
                    break;
                switch (reader.getFieldNumber()) {
                    case 1:
                        message.Current = reader.readInt32();
                        break;
                    case 2:
                        message.Size = reader.readInt32();
                        break;
                    case 3:
                        message.Total = reader.readInt32();
                        break;
                    default: reader.skipField();
                }
            }
            return message;
        }
        serializeBinary(): Uint8Array {
            return this.serialize();
        }
        static deserializeBinary(bytes: Uint8Array): Pagination {
            return Pagination.deserialize(bytes);
        }
    }
    export class Header extends pb_1.Message {
        constructor(data?: any[] | {
            id?: number;
            sequence?: number;
            ULID?: string;
            hostname?: string;
        }) {
            super();
            pb_1.Message.initialize(this, Array.isArray(data) ? data : [], 0, -1, [], []);
            if (!Array.isArray(data) && typeof data == "object") {
                if ("id" in data && data.id != undefined) {
                    this.id = data.id;
                }
                if ("sequence" in data && data.sequence != undefined) {
                    this.sequence = data.sequence;
                }
                if ("ULID" in data && data.ULID != undefined) {
                    this.ULID = data.ULID;
                }
                if ("hostname" in data && data.hostname != undefined) {
                    this.hostname = data.hostname;
                }
            }
        }
        get id() {
            return pb_1.Message.getField(this, 1) as number;
        }
        set id(value: number) {
            pb_1.Message.setField(this, 1, value);
        }
        get sequence() {
            return pb_1.Message.getField(this, 2) as number;
        }
        set sequence(value: number) {
            pb_1.Message.setField(this, 2, value);
        }
        get ULID() {
            return pb_1.Message.getField(this, 3) as string;
        }
        set ULID(value: string) {
            pb_1.Message.setField(this, 3, value);
        }
        get hostname() {
            return pb_1.Message.getField(this, 4) as string;
        }
        set hostname(value: string) {
            pb_1.Message.setField(this, 4, value);
        }
        static fromObject(data: {
            id?: number;
            sequence?: number;
            ULID?: string;
            hostname?: string;
        }) {
            const message = new Header({});
            if (data.id != null) {
                message.id = data.id;
            }
            if (data.sequence != null) {
                message.sequence = data.sequence;
            }
            if (data.ULID != null) {
                message.ULID = data.ULID;
            }
            if (data.hostname != null) {
                message.hostname = data.hostname;
            }
            return message;
        }
        toObject() {
            const data: {
                id?: number;
                sequence?: number;
                ULID?: string;
                hostname?: string;
            } = {};
            if (this.id != null) {
                data.id = this.id;
            }
            if (this.sequence != null) {
                data.sequence = this.sequence;
            }
            if (this.ULID != null) {
                data.ULID = this.ULID;
            }
            if (this.hostname != null) {
                data.hostname = this.hostname;
            }
            return data;
        }
        serialize(): Uint8Array;
        serialize(w: pb_1.BinaryWriter): void;
        serialize(w?: pb_1.BinaryWriter): Uint8Array | void {
            const writer = w || new pb_1.BinaryWriter();
            if (this.id !== undefined)
                writer.writeInt64(1, this.id);
            if (this.sequence !== undefined)
                writer.writeInt64(2, this.sequence);
            if (typeof this.ULID === "string" && this.ULID.length)
                writer.writeString(3, this.ULID);
            if (typeof this.hostname === "string" && this.hostname.length)
                writer.writeString(4, this.hostname);
            if (!w)
                return writer.getResultBuffer();
        }
        static deserialize(bytes: Uint8Array | pb_1.BinaryReader): Header {
            const reader = bytes instanceof pb_1.BinaryReader ? bytes : new pb_1.BinaryReader(bytes), message = new Header();
            while (reader.nextField()) {
                if (reader.isEndGroup())
                    break;
                switch (reader.getFieldNumber()) {
                    case 1:
                        message.id = reader.readInt64();
                        break;
                    case 2:
                        message.sequence = reader.readInt64();
                        break;
                    case 3:
                        message.ULID = reader.readString();
                        break;
                    case 4:
                        message.hostname = reader.readString();
                        break;
                    default: reader.skipField();
                }
            }
            return message;
        }
        serializeBinary(): Uint8Array {
            return this.serialize();
        }
        static deserializeBinary(bytes: Uint8Array): Header {
            return Header.deserialize(bytes);
        }
    }
    export class RequestHeader extends pb_1.Message {
        constructor(data?: any[] | {
            base?: Header;
            method?: ModelCRUD;
        }) {
            super();
            pb_1.Message.initialize(this, Array.isArray(data) ? data : [], 0, -1, [], []);
            if (!Array.isArray(data) && typeof data == "object") {
                if ("base" in data && data.base != undefined) {
                    this.base = data.base;
                }
                if ("method" in data && data.method != undefined) {
                    this.method = data.method;
                }
            }
        }
        get base() {
            return pb_1.Message.getWrapperField(this, Header, 1) as Header;
        }
        set base(value: Header) {
            pb_1.Message.setWrapperField(this, 1, value);
        }
        get method() {
            return pb_1.Message.getField(this, 2) as ModelCRUD;
        }
        set method(value: ModelCRUD) {
            pb_1.Message.setField(this, 2, value);
        }
        static fromObject(data: {
            base?: ReturnType<typeof Header.prototype.toObject>;
            method?: ModelCRUD;
        }) {
            const message = new RequestHeader({});
            if (data.base != null) {
                message.base = Header.fromObject(data.base);
            }
            if (data.method != null) {
                message.method = data.method;
            }
            return message;
        }
        toObject() {
            const data: {
                base?: ReturnType<typeof Header.prototype.toObject>;
                method?: ModelCRUD;
            } = {};
            if (this.base != null) {
                data.base = this.base.toObject();
            }
            if (this.method != null) {
                data.method = this.method;
            }
            return data;
        }
        serialize(): Uint8Array;
        serialize(w: pb_1.BinaryWriter): void;
        serialize(w?: pb_1.BinaryWriter): Uint8Array | void {
            const writer = w || new pb_1.BinaryWriter();
            if (this.base !== undefined)
                writer.writeMessage(1, this.base, () => this.base.serialize(writer));
            if (this.method !== undefined)
                writer.writeEnum(2, this.method);
            if (!w)
                return writer.getResultBuffer();
        }
        static deserialize(bytes: Uint8Array | pb_1.BinaryReader): RequestHeader {
            const reader = bytes instanceof pb_1.BinaryReader ? bytes : new pb_1.BinaryReader(bytes), message = new RequestHeader();
            while (reader.nextField()) {
                if (reader.isEndGroup())
                    break;
                switch (reader.getFieldNumber()) {
                    case 1:
                        reader.readMessage(message.base, () => message.base = Header.deserialize(reader));
                        break;
                    case 2:
                        message.method = reader.readEnum();
                        break;
                    default: reader.skipField();
                }
            }
            return message;
        }
        serializeBinary(): Uint8Array {
            return this.serialize();
        }
        static deserializeBinary(bytes: Uint8Array): RequestHeader {
            return RequestHeader.deserialize(bytes);
        }
    }
    export class ResponseHeader extends pb_1.Message {
        constructor(data?: any[] | {
            base?: Header;
            error?: string;
            replyULID?: string;
        }) {
            super();
            pb_1.Message.initialize(this, Array.isArray(data) ? data : [], 0, -1, [], []);
            if (!Array.isArray(data) && typeof data == "object") {
                if ("base" in data && data.base != undefined) {
                    this.base = data.base;
                }
                if ("error" in data && data.error != undefined) {
                    this.error = data.error;
                }
                if ("replyULID" in data && data.replyULID != undefined) {
                    this.replyULID = data.replyULID;
                }
            }
        }
        get base() {
            return pb_1.Message.getWrapperField(this, Header, 1) as Header;
        }
        set base(value: Header) {
            pb_1.Message.setWrapperField(this, 1, value);
        }
        get error() {
            return pb_1.Message.getField(this, 2) as string;
        }
        set error(value: string) {
            pb_1.Message.setField(this, 2, value);
        }
        get replyULID() {
            return pb_1.Message.getField(this, 3) as string;
        }
        set replyULID(value: string) {
            pb_1.Message.setField(this, 3, value);
        }
        static fromObject(data: {
            base?: ReturnType<typeof Header.prototype.toObject>;
            error?: string;
            replyULID?: string;
        }) {
            const message = new ResponseHeader({});
            if (data.base != null) {
                message.base = Header.fromObject(data.base);
            }
            if (data.error != null) {
                message.error = data.error;
            }
            if (data.replyULID != null) {
                message.replyULID = data.replyULID;
            }
            return message;
        }
        toObject() {
            const data: {
                base?: ReturnType<typeof Header.prototype.toObject>;
                error?: string;
                replyULID?: string;
            } = {};
            if (this.base != null) {
                data.base = this.base.toObject();
            }
            if (this.error != null) {
                data.error = this.error;
            }
            if (this.replyULID != null) {
                data.replyULID = this.replyULID;
            }
            return data;
        }
        serialize(): Uint8Array;
        serialize(w: pb_1.BinaryWriter): void;
        serialize(w?: pb_1.BinaryWriter): Uint8Array | void {
            const writer = w || new pb_1.BinaryWriter();
            if (this.base !== undefined)
                writer.writeMessage(1, this.base, () => this.base.serialize(writer));
            if (typeof this.error === "string" && this.error.length)
                writer.writeString(2, this.error);
            if (typeof this.replyULID === "string" && this.replyULID.length)
                writer.writeString(3, this.replyULID);
            if (!w)
                return writer.getResultBuffer();
        }
        static deserialize(bytes: Uint8Array | pb_1.BinaryReader): ResponseHeader {
            const reader = bytes instanceof pb_1.BinaryReader ? bytes : new pb_1.BinaryReader(bytes), message = new ResponseHeader();
            while (reader.nextField()) {
                if (reader.isEndGroup())
                    break;
                switch (reader.getFieldNumber()) {
                    case 1:
                        reader.readMessage(message.base, () => message.base = Header.deserialize(reader));
                        break;
                    case 2:
                        message.error = reader.readString();
                        break;
                    case 3:
                        message.replyULID = reader.readString();
                        break;
                    default: reader.skipField();
                }
            }
            return message;
        }
        serializeBinary(): Uint8Array {
            return this.serialize();
        }
        static deserializeBinary(bytes: Uint8Array): ResponseHeader {
            return ResponseHeader.deserialize(bytes);
        }
    }
}
