import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class Org extends jspb.Message {
  getId(): string;
  setId(value: string): Org;

  getName(): string;
  setName(value: string): Org;

  getEmail(): string;
  setEmail(value: string): Org;

  getAdmin(): string;
  setAdmin(value: string): Org;

  getApiid(): string;
  setApiid(value: string): Org;

  getApikey(): string;
  setApikey(value: string): Org;

  getLogodata(): string;
  setLogodata(value: string): Org;

  getLocation(): string;
  setLocation(value: string): Org;

  getType(): common_pb.Type;
  setType(value: common_pb.Type): Org;

  getStatus(): common_pb.Status;
  setStatus(value: common_pb.Status): Org;

  getTrailexpirationdate(): number;
  setTrailexpirationdate(value: number): Org;

  getPayload(): string;
  setPayload(value: string): Org;

  getNotesid(): string;
  setNotesid(value: string): Org;

  getCreatedby(): string;
  setCreatedby(value: string): Org;

  getCreatedat(): number;
  setCreatedat(value: number): Org;

  getUpdatedat(): number;
  setUpdatedat(value: number): Org;

  getDeletedat(): number;
  setDeletedat(value: number): Org;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Org.AsObject;
  static toObject(includeInstance: boolean, msg: Org): Org.AsObject;
  static serializeBinaryToWriter(message: Org, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Org;
  static deserializeBinaryFromReader(message: Org, reader: jspb.BinaryReader): Org;
}

export namespace Org {
  export type AsObject = {
    id: string,
    name: string,
    email: string,
    admin: string,
    apiid: string,
    apikey: string,
    logodata: string,
    location: string,
    type: common_pb.Type,
    status: common_pb.Status,
    trailexpirationdate: number,
    payload: string,
    notesid: string,
    createdby: string,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }
}

export class OrgRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): OrgRequest;
  hasHdr(): boolean;
  clearHdr(): OrgRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): OrgRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): OrgRequest;
  hasPagination(): boolean;
  clearPagination(): OrgRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): OrgRequest;
  hasFilter(): boolean;
  clearFilter(): OrgRequest;

  getOrg(): Org | undefined;
  setOrg(value?: Org): OrgRequest;
  hasOrg(): boolean;
  clearOrg(): OrgRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrgRequest.AsObject;
  static toObject(includeInstance: boolean, msg: OrgRequest): OrgRequest.AsObject;
  static serializeBinaryToWriter(message: OrgRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrgRequest;
  static deserializeBinaryFromReader(message: OrgRequest, reader: jspb.BinaryReader): OrgRequest;
}

export namespace OrgRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    org?: Org.AsObject,
  }
}

export class OrgResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): OrgResponse;
  hasHdr(): boolean;
  clearHdr(): OrgResponse;

  getError(): string;
  setError(value: string): OrgResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): OrgResponse;
  hasPagination(): boolean;
  clearPagination(): OrgResponse;

  getOrgsList(): Array<Org>;
  setOrgsList(value: Array<Org>): OrgResponse;
  clearOrgsList(): OrgResponse;
  addOrgs(value?: Org, index?: number): Org;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrgResponse.AsObject;
  static toObject(includeInstance: boolean, msg: OrgResponse): OrgResponse.AsObject;
  static serializeBinaryToWriter(message: OrgResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrgResponse;
  static deserializeBinaryFromReader(message: OrgResponse, reader: jspb.BinaryReader): OrgResponse;
}

export namespace OrgResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    orgsList: Array<Org.AsObject>,
  }
}

export class OrgMeta extends jspb.Message {
  getId(): string;
  setId(value: string): OrgMeta;

  getOrgid(): string;
  setOrgid(value: string): OrgMeta;

  getPayload(): string;
  setPayload(value: string): OrgMeta;

  getNotesUuid(): string;
  setNotesUuid(value: string): OrgMeta;

  getUpdatedAt(): string;
  setUpdatedAt(value: string): OrgMeta;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrgMeta.AsObject;
  static toObject(includeInstance: boolean, msg: OrgMeta): OrgMeta.AsObject;
  static serializeBinaryToWriter(message: OrgMeta, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrgMeta;
  static deserializeBinaryFromReader(message: OrgMeta, reader: jspb.BinaryReader): OrgMeta;
}

export namespace OrgMeta {
  export type AsObject = {
    id: string,
    orgid: string,
    payload: string,
    notesUuid: string,
    updatedAt: string,
  }
}

export class OrgMetaRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): OrgMetaRequest;
  hasHdr(): boolean;
  clearHdr(): OrgMetaRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): OrgMetaRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): OrgMetaRequest;
  hasFilter(): boolean;
  clearFilter(): OrgMetaRequest;

  getOrgmeta(): OrgMeta | undefined;
  setOrgmeta(value?: OrgMeta): OrgMetaRequest;
  hasOrgmeta(): boolean;
  clearOrgmeta(): OrgMetaRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrgMetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: OrgMetaRequest): OrgMetaRequest.AsObject;
  static serializeBinaryToWriter(message: OrgMetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrgMetaRequest;
  static deserializeBinaryFromReader(message: OrgMetaRequest, reader: jspb.BinaryReader): OrgMetaRequest;
}

export namespace OrgMetaRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    orgmeta?: OrgMeta.AsObject,
  }
}

export class OrgMetaResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): OrgMetaResponse;
  hasHdr(): boolean;
  clearHdr(): OrgMetaResponse;

  getError(): string;
  setError(value: string): OrgMetaResponse;

  getOrgmeta(): OrgMeta | undefined;
  setOrgmeta(value?: OrgMeta): OrgMetaResponse;
  hasOrgmeta(): boolean;
  clearOrgmeta(): OrgMetaResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrgMetaResponse.AsObject;
  static toObject(includeInstance: boolean, msg: OrgMetaResponse): OrgMetaResponse.AsObject;
  static serializeBinaryToWriter(message: OrgMetaResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrgMetaResponse;
  static deserializeBinaryFromReader(message: OrgMetaResponse, reader: jspb.BinaryReader): OrgMetaResponse;
}

export namespace OrgMetaResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    orgmeta?: OrgMeta.AsObject,
  }
}

