/**
 * @fileoverview gRPC-Web generated client stub for user_meta
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var common_pb = require('./common_pb.js')
const proto = {};
proto.user_meta = require('./user_meta_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.user_meta.UserMetaServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?grpc.web.ClientOptions} options
 * @constructor
 * @struct
 * @final
 */
proto.user_meta.UserMetaServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options.format = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user_meta.UserMetaRequest,
 *   !proto.user_meta.UserMetaResponse>}
 */
const methodDescriptor_UserMetaService_Select = new grpc.web.MethodDescriptor(
  '/user_meta.UserMetaService/Select',
  grpc.web.MethodType.UNARY,
  proto.user_meta.UserMetaRequest,
  proto.user_meta.UserMetaResponse,
  /**
   * @param {!proto.user_meta.UserMetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user_meta.UserMetaResponse.deserializeBinary
);


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user_meta.UserMetaResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user_meta.UserMetaResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user_meta.UserMetaServiceClient.prototype.select =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user_meta.UserMetaService/Select',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Select,
      callback);
};


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user_meta.UserMetaResponse>}
 *     Promise that resolves to the response
 */
proto.user_meta.UserMetaServicePromiseClient.prototype.select =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user_meta.UserMetaService/Select',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Select);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user_meta.UserMetaRequest,
 *   !proto.user_meta.UserMetaResponse>}
 */
const methodDescriptor_UserMetaService_Create = new grpc.web.MethodDescriptor(
  '/user_meta.UserMetaService/Create',
  grpc.web.MethodType.UNARY,
  proto.user_meta.UserMetaRequest,
  proto.user_meta.UserMetaResponse,
  /**
   * @param {!proto.user_meta.UserMetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user_meta.UserMetaResponse.deserializeBinary
);


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user_meta.UserMetaResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user_meta.UserMetaResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user_meta.UserMetaServiceClient.prototype.create =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user_meta.UserMetaService/Create',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Create,
      callback);
};


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user_meta.UserMetaResponse>}
 *     Promise that resolves to the response
 */
proto.user_meta.UserMetaServicePromiseClient.prototype.create =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user_meta.UserMetaService/Create',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Create);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user_meta.UserMetaRequest,
 *   !proto.user_meta.UserMetaResponse>}
 */
const methodDescriptor_UserMetaService_Update = new grpc.web.MethodDescriptor(
  '/user_meta.UserMetaService/Update',
  grpc.web.MethodType.UNARY,
  proto.user_meta.UserMetaRequest,
  proto.user_meta.UserMetaResponse,
  /**
   * @param {!proto.user_meta.UserMetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user_meta.UserMetaResponse.deserializeBinary
);


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user_meta.UserMetaResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user_meta.UserMetaResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user_meta.UserMetaServiceClient.prototype.update =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user_meta.UserMetaService/Update',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Update,
      callback);
};


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user_meta.UserMetaResponse>}
 *     Promise that resolves to the response
 */
proto.user_meta.UserMetaServicePromiseClient.prototype.update =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user_meta.UserMetaService/Update',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Update);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.user_meta.UserMetaRequest,
 *   !proto.user_meta.UserMetaResponse>}
 */
const methodDescriptor_UserMetaService_Delete = new grpc.web.MethodDescriptor(
  '/user_meta.UserMetaService/Delete',
  grpc.web.MethodType.UNARY,
  proto.user_meta.UserMetaRequest,
  proto.user_meta.UserMetaResponse,
  /**
   * @param {!proto.user_meta.UserMetaRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.user_meta.UserMetaResponse.deserializeBinary
);


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.RpcError, ?proto.user_meta.UserMetaResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.user_meta.UserMetaResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.user_meta.UserMetaServiceClient.prototype.delete =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/user_meta.UserMetaService/Delete',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Delete,
      callback);
};


/**
 * @param {!proto.user_meta.UserMetaRequest} request The
 *     request proto
 * @param {?Object<string, string>=} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.user_meta.UserMetaResponse>}
 *     Promise that resolves to the response
 */
proto.user_meta.UserMetaServicePromiseClient.prototype.delete =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/user_meta.UserMetaService/Delete',
      request,
      metadata || {},
      methodDescriptor_UserMetaService_Delete);
};


module.exports = proto.user_meta;

