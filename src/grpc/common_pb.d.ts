import * as jspb from 'google-protobuf'



export class Filter extends jspb.Message {
  getRaw(): string;
  setRaw(value: string): Filter;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Filter.AsObject;
  static toObject(includeInstance: boolean, msg: Filter): Filter.AsObject;
  static serializeBinaryToWriter(message: Filter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Filter;
  static deserializeBinaryFromReader(message: Filter, reader: jspb.BinaryReader): Filter;
}

export namespace Filter {
  export type AsObject = {
    raw: string,
  }
}

export class Pagination extends jspb.Message {
  getCurrent(): number;
  setCurrent(value: number): Pagination;

  getSize(): number;
  setSize(value: number): Pagination;

  getTotal(): number;
  setTotal(value: number): Pagination;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Pagination.AsObject;
  static toObject(includeInstance: boolean, msg: Pagination): Pagination.AsObject;
  static serializeBinaryToWriter(message: Pagination, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Pagination;
  static deserializeBinaryFromReader(message: Pagination, reader: jspb.BinaryReader): Pagination;
}

export namespace Pagination {
  export type AsObject = {
    current: number,
    size: number,
    total: number,
  }
}

export class Header extends jspb.Message {
  getId(): number;
  setId(value: number): Header;

  getSequence(): number;
  setSequence(value: number): Header;

  getUlid(): string;
  setUlid(value: string): Header;

  getHostname(): string;
  setHostname(value: string): Header;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Header.AsObject;
  static toObject(includeInstance: boolean, msg: Header): Header.AsObject;
  static serializeBinaryToWriter(message: Header, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Header;
  static deserializeBinaryFromReader(message: Header, reader: jspb.BinaryReader): Header;
}

export namespace Header {
  export type AsObject = {
    id: number,
    sequence: number,
    ulid: string,
    hostname: string,
  }
}

export class RequestHeader extends jspb.Message {
  getBase(): Header | undefined;
  setBase(value?: Header): RequestHeader;
  hasBase(): boolean;
  clearBase(): RequestHeader;

  getMethod(): ModelCRUD;
  setMethod(value: ModelCRUD): RequestHeader;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RequestHeader.AsObject;
  static toObject(includeInstance: boolean, msg: RequestHeader): RequestHeader.AsObject;
  static serializeBinaryToWriter(message: RequestHeader, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RequestHeader;
  static deserializeBinaryFromReader(message: RequestHeader, reader: jspb.BinaryReader): RequestHeader;
}

export namespace RequestHeader {
  export type AsObject = {
    base?: Header.AsObject,
    method: ModelCRUD,
  }
}

export class ResponseHeader extends jspb.Message {
  getBase(): Header | undefined;
  setBase(value?: Header): ResponseHeader;
  hasBase(): boolean;
  clearBase(): ResponseHeader;

  getError(): string;
  setError(value: string): ResponseHeader;

  getReplyulid(): string;
  setReplyulid(value: string): ResponseHeader;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResponseHeader.AsObject;
  static toObject(includeInstance: boolean, msg: ResponseHeader): ResponseHeader.AsObject;
  static serializeBinaryToWriter(message: ResponseHeader, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResponseHeader;
  static deserializeBinaryFromReader(message: ResponseHeader, reader: jspb.BinaryReader): ResponseHeader;
}

export namespace ResponseHeader {
  export type AsObject = {
    base?: Header.AsObject,
    error: string,
    replyulid: string,
  }
}

export enum Role { 
  ADMIN = 0,
  MODERATOR = 1,
  READONLY = 2,
}
export enum Type { 
  TRAIL = 0,
  BASIC = 1,
  PREMIUM = 2,
}
export enum Status { 
  ACTIVE = 0,
  EXPIRED = 2,
  DEACTIVATED = 3,
  BLOCKED = 4,
}
export enum ModelCRUD { 
  CREATE = 0,
  SELECT = 1,
  SELECTALL = 2,
  UPDATE = 3,
  DELETE = 4,
  SEARCH = 5,
  STALE = 6,
  ENROL = 7,
  ENROLLED = 8,
  UPSERT = 9,
  CURRENT = 10,
  ACTION = 11,
}
