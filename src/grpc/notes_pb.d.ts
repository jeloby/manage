import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class Notes extends jspb.Message {
  getId(): string;
  setId(value: string): Notes;

  getDescription(): string;
  setDescription(value: string): Notes;

  getNotes(): string;
  setNotes(value: string): Notes;

  getUpdatedat(): number;
  setUpdatedat(value: number): Notes;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Notes.AsObject;
  static toObject(includeInstance: boolean, msg: Notes): Notes.AsObject;
  static serializeBinaryToWriter(message: Notes, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Notes;
  static deserializeBinaryFromReader(message: Notes, reader: jspb.BinaryReader): Notes;
}

export namespace Notes {
  export type AsObject = {
    id: string,
    description: string,
    notes: string,
    updatedat: number,
  }
}

export class NoteRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): NoteRequest;
  hasHdr(): boolean;
  clearHdr(): NoteRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): NoteRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): NoteRequest;
  hasFilter(): boolean;
  clearFilter(): NoteRequest;

  getNotes(): Notes | undefined;
  setNotes(value?: Notes): NoteRequest;
  hasNotes(): boolean;
  clearNotes(): NoteRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NoteRequest.AsObject;
  static toObject(includeInstance: boolean, msg: NoteRequest): NoteRequest.AsObject;
  static serializeBinaryToWriter(message: NoteRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NoteRequest;
  static deserializeBinaryFromReader(message: NoteRequest, reader: jspb.BinaryReader): NoteRequest;
}

export namespace NoteRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    notes?: Notes.AsObject,
  }
}

export class NoteResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): NoteResponse;
  hasHdr(): boolean;
  clearHdr(): NoteResponse;

  getError(): string;
  setError(value: string): NoteResponse;

  getNotes(): Notes | undefined;
  setNotes(value?: Notes): NoteResponse;
  hasNotes(): boolean;
  clearNotes(): NoteResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NoteResponse.AsObject;
  static toObject(includeInstance: boolean, msg: NoteResponse): NoteResponse.AsObject;
  static serializeBinaryToWriter(message: NoteResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NoteResponse;
  static deserializeBinaryFromReader(message: NoteResponse, reader: jspb.BinaryReader): NoteResponse;
}

export namespace NoteResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    notes?: Notes.AsObject,
  }
}

