import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class User extends jspb.Message {
  getId(): string;
  setId(value: string): User;

  getOrgid(): string;
  setOrgid(value: string): User;

  getFirstname(): string;
  setFirstname(value: string): User;

  getLastname(): string;
  setLastname(value: string): User;

  getEmail(): string;
  setEmail(value: string): User;

  getPhone(): string;
  setPhone(value: string): User;

  getRole(): common_pb.Role;
  setRole(value: common_pb.Role): User;

  getPassword(): string;
  setPassword(value: string): User;

  getPasswordupdatedat(): number;
  setPasswordupdatedat(value: number): User;

  getTrailexpirationdate(): number;
  setTrailexpirationdate(value: number): User;

  getPayload(): string;
  setPayload(value: string): User;

  getNotesid(): string;
  setNotesid(value: string): User;

  getCreatedby(): string;
  setCreatedby(value: string): User;

  getCreatedat(): number;
  setCreatedat(value: number): User;

  getUpdatedat(): number;
  setUpdatedat(value: number): User;

  getDeletedat(): number;
  setDeletedat(value: number): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): User.AsObject;
  static toObject(includeInstance: boolean, msg: User): User.AsObject;
  static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): User;
  static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
  export type AsObject = {
    id: string,
    orgid: string,
    firstname: string,
    lastname: string,
    email: string,
    phone: string,
    role: common_pb.Role,
    password: string,
    passwordupdatedat: number,
    trailexpirationdate: number,
    payload: string,
    notesid: string,
    createdby: string,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }
}

export class UserRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): UserRequest;
  hasHdr(): boolean;
  clearHdr(): UserRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): UserRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): UserRequest;
  hasPagination(): boolean;
  clearPagination(): UserRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): UserRequest;
  hasFilter(): boolean;
  clearFilter(): UserRequest;

  getUser(): User | undefined;
  setUser(value?: User): UserRequest;
  hasUser(): boolean;
  clearUser(): UserRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UserRequest): UserRequest.AsObject;
  static serializeBinaryToWriter(message: UserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserRequest;
  static deserializeBinaryFromReader(message: UserRequest, reader: jspb.BinaryReader): UserRequest;
}

export namespace UserRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    user?: User.AsObject,
  }
}

export class UserResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): UserResponse;
  hasHdr(): boolean;
  clearHdr(): UserResponse;

  getError(): string;
  setError(value: string): UserResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): UserResponse;
  hasPagination(): boolean;
  clearPagination(): UserResponse;

  getUsersList(): Array<User>;
  setUsersList(value: Array<User>): UserResponse;
  clearUsersList(): UserResponse;
  addUsers(value?: User, index?: number): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UserResponse): UserResponse.AsObject;
  static serializeBinaryToWriter(message: UserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserResponse;
  static deserializeBinaryFromReader(message: UserResponse, reader: jspb.BinaryReader): UserResponse;
}

export namespace UserResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    usersList: Array<User.AsObject>,
  }
}

