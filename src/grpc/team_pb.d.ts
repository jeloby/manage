import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class Team extends jspb.Message {
  getId(): string;
  setId(value: string): Team;

  getOrgid(): string;
  setOrgid(value: string): Team;

  getName(): string;
  setName(value: string): Team;

  getLocation(): string;
  setLocation(value: string): Team;

  getAdmin(): string;
  setAdmin(value: string): Team;

  getTeamType(): string;
  setTeamType(value: string): Team;

  getTeamStatus(): common_pb.Status;
  setTeamStatus(value: common_pb.Status): Team;

  getPayload(): number;
  setPayload(value: number): Team;

  getNotesid(): string;
  setNotesid(value: string): Team;

  getCreatedby(): string;
  setCreatedby(value: string): Team;

  getCreatedat(): number;
  setCreatedat(value: number): Team;

  getUpdatedat(): number;
  setUpdatedat(value: number): Team;

  getDeletedat(): number;
  setDeletedat(value: number): Team;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Team.AsObject;
  static toObject(includeInstance: boolean, msg: Team): Team.AsObject;
  static serializeBinaryToWriter(message: Team, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Team;
  static deserializeBinaryFromReader(message: Team, reader: jspb.BinaryReader): Team;
}

export namespace Team {
  export type AsObject = {
    id: string,
    orgid: string,
    name: string,
    location: string,
    admin: string,
    teamType: string,
    teamStatus: common_pb.Status,
    payload: number,
    notesid: string,
    createdby: string,
    createdat: number,
    updatedat: number,
    deletedat: number,
  }
}

export class TeamRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): TeamRequest;
  hasHdr(): boolean;
  clearHdr(): TeamRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): TeamRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TeamRequest;
  hasPagination(): boolean;
  clearPagination(): TeamRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): TeamRequest;
  hasFilter(): boolean;
  clearFilter(): TeamRequest;

  getTeam(): Team | undefined;
  setTeam(value?: Team): TeamRequest;
  hasTeam(): boolean;
  clearTeam(): TeamRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TeamRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TeamRequest): TeamRequest.AsObject;
  static serializeBinaryToWriter(message: TeamRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TeamRequest;
  static deserializeBinaryFromReader(message: TeamRequest, reader: jspb.BinaryReader): TeamRequest;
}

export namespace TeamRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    team?: Team.AsObject,
  }
}

export class TeamResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): TeamResponse;
  hasHdr(): boolean;
  clearHdr(): TeamResponse;

  getError(): string;
  setError(value: string): TeamResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TeamResponse;
  hasPagination(): boolean;
  clearPagination(): TeamResponse;

  getTeamsList(): Array<Team>;
  setTeamsList(value: Array<Team>): TeamResponse;
  clearTeamsList(): TeamResponse;
  addTeams(value?: Team, index?: number): Team;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TeamResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TeamResponse): TeamResponse.AsObject;
  static serializeBinaryToWriter(message: TeamResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TeamResponse;
  static deserializeBinaryFromReader(message: TeamResponse, reader: jspb.BinaryReader): TeamResponse;
}

export namespace TeamResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    teamsList: Array<Team.AsObject>,
  }
}

export class TeamMeta extends jspb.Message {
  getId(): string;
  setId(value: string): TeamMeta;

  getTeamid(): string;
  setTeamid(value: string): TeamMeta;

  getPayload(): number;
  setPayload(value: number): TeamMeta;

  getNotesid(): string;
  setNotesid(value: string): TeamMeta;

  getUpdatedat(): number;
  setUpdatedat(value: number): TeamMeta;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TeamMeta.AsObject;
  static toObject(includeInstance: boolean, msg: TeamMeta): TeamMeta.AsObject;
  static serializeBinaryToWriter(message: TeamMeta, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TeamMeta;
  static deserializeBinaryFromReader(message: TeamMeta, reader: jspb.BinaryReader): TeamMeta;
}

export namespace TeamMeta {
  export type AsObject = {
    id: string,
    teamid: string,
    payload: number,
    notesid: string,
    updatedat: number,
  }
}

export class TeamMetaRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): TeamMetaRequest;
  hasHdr(): boolean;
  clearHdr(): TeamMetaRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): TeamMetaRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): TeamMetaRequest;
  hasFilter(): boolean;
  clearFilter(): TeamMetaRequest;

  getTeammeta(): TeamMeta | undefined;
  setTeammeta(value?: TeamMeta): TeamMetaRequest;
  hasTeammeta(): boolean;
  clearTeammeta(): TeamMetaRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TeamMetaRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TeamMetaRequest): TeamMetaRequest.AsObject;
  static serializeBinaryToWriter(message: TeamMetaRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TeamMetaRequest;
  static deserializeBinaryFromReader(message: TeamMetaRequest, reader: jspb.BinaryReader): TeamMetaRequest;
}

export namespace TeamMetaRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    filter?: common_pb.Filter.AsObject,
    teammeta?: TeamMeta.AsObject,
  }
}

export class TeamMetaResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): TeamMetaResponse;
  hasHdr(): boolean;
  clearHdr(): TeamMetaResponse;

  getError(): string;
  setError(value: string): TeamMetaResponse;

  getTeammeta(): TeamMeta | undefined;
  setTeammeta(value?: TeamMeta): TeamMetaResponse;
  hasTeammeta(): boolean;
  clearTeammeta(): TeamMetaResponse;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TeamMetaResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TeamMetaResponse): TeamMetaResponse.AsObject;
  static serializeBinaryToWriter(message: TeamMetaResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TeamMetaResponse;
  static deserializeBinaryFromReader(message: TeamMetaResponse, reader: jspb.BinaryReader): TeamMetaResponse;
}

export namespace TeamMetaResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    teammeta?: TeamMeta.AsObject,
  }
}

