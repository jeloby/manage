/**
 * @fileoverview gRPC-Web generated client stub for user_meta
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as user_meta_pb from './user_meta_pb';


export class UserMetaServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoSelect = new grpcWeb.MethodDescriptor(
    '/user_meta.UserMetaService/Select',
    grpcWeb.MethodType.UNARY,
    user_meta_pb.UserMetaRequest,
    user_meta_pb.UserMetaResponse,
    (request: user_meta_pb.UserMetaRequest) => {
      return request.serializeBinary();
    },
    user_meta_pb.UserMetaResponse.deserializeBinary
  );

  select(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_meta_pb.UserMetaResponse>;

  select(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void): grpcWeb.ClientReadableStream<user_meta_pb.UserMetaResponse>;

  select(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user_meta.UserMetaService/Select',
        request,
        metadata || {},
        this.methodInfoSelect,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user_meta.UserMetaService/Select',
    request,
    metadata || {},
    this.methodInfoSelect);
  }

  methodInfoCreate = new grpcWeb.MethodDescriptor(
    '/user_meta.UserMetaService/Create',
    grpcWeb.MethodType.UNARY,
    user_meta_pb.UserMetaRequest,
    user_meta_pb.UserMetaResponse,
    (request: user_meta_pb.UserMetaRequest) => {
      return request.serializeBinary();
    },
    user_meta_pb.UserMetaResponse.deserializeBinary
  );

  create(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_meta_pb.UserMetaResponse>;

  create(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void): grpcWeb.ClientReadableStream<user_meta_pb.UserMetaResponse>;

  create(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user_meta.UserMetaService/Create',
        request,
        metadata || {},
        this.methodInfoCreate,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user_meta.UserMetaService/Create',
    request,
    metadata || {},
    this.methodInfoCreate);
  }

  methodInfoUpdate = new grpcWeb.MethodDescriptor(
    '/user_meta.UserMetaService/Update',
    grpcWeb.MethodType.UNARY,
    user_meta_pb.UserMetaRequest,
    user_meta_pb.UserMetaResponse,
    (request: user_meta_pb.UserMetaRequest) => {
      return request.serializeBinary();
    },
    user_meta_pb.UserMetaResponse.deserializeBinary
  );

  update(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_meta_pb.UserMetaResponse>;

  update(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void): grpcWeb.ClientReadableStream<user_meta_pb.UserMetaResponse>;

  update(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user_meta.UserMetaService/Update',
        request,
        metadata || {},
        this.methodInfoUpdate,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user_meta.UserMetaService/Update',
    request,
    metadata || {},
    this.methodInfoUpdate);
  }

  methodInfoDelete = new grpcWeb.MethodDescriptor(
    '/user_meta.UserMetaService/Delete',
    grpcWeb.MethodType.UNARY,
    user_meta_pb.UserMetaRequest,
    user_meta_pb.UserMetaResponse,
    (request: user_meta_pb.UserMetaRequest) => {
      return request.serializeBinary();
    },
    user_meta_pb.UserMetaResponse.deserializeBinary
  );

  delete(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null): Promise<user_meta_pb.UserMetaResponse>;

  delete(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void): grpcWeb.ClientReadableStream<user_meta_pb.UserMetaResponse>;

  delete(
    request: user_meta_pb.UserMetaRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: user_meta_pb.UserMetaResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/user_meta.UserMetaService/Delete',
        request,
        metadata || {},
        this.methodInfoDelete,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/user_meta.UserMetaService/Delete',
    request,
    metadata || {},
    this.methodInfoDelete);
  }

}

