import * as jspb from 'google-protobuf'

import * as common_pb from './common_pb';


export class Ticket extends jspb.Message {
  getId(): string;
  setId(value: string): Ticket;

  getType(): Type;
  setType(value: Type): Ticket;

  getCompanyid(): string;
  setCompanyid(value: string): Ticket;

  getUserid(): string;
  setUserid(value: string): Ticket;

  getCustomeruser(): string;
  setCustomeruser(value: string): Ticket;

  getCustomercompany(): string;
  setCustomercompany(value: string): Ticket;

  getCustomercompanyid(): string;
  setCustomercompanyid(value: string): Ticket;

  getTitle(): string;
  setTitle(value: string): Ticket;

  getDescription(): string;
  setDescription(value: string): Ticket;

  getStatus(): Status;
  setStatus(value: Status): Ticket;

  getTimestamp(): number;
  setTimestamp(value: number): Ticket;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Ticket.AsObject;
  static toObject(includeInstance: boolean, msg: Ticket): Ticket.AsObject;
  static serializeBinaryToWriter(message: Ticket, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Ticket;
  static deserializeBinaryFromReader(message: Ticket, reader: jspb.BinaryReader): Ticket;
}

export namespace Ticket {
  export type AsObject = {
    id: string,
    type: Type,
    companyid: string,
    userid: string,
    customeruser: string,
    customercompany: string,
    customercompanyid: string,
    title: string,
    description: string,
    status: Status,
    timestamp: number,
  }
}

export class TicketComment extends jspb.Message {
  getId(): string;
  setId(value: string): TicketComment;

  getTicketid(): string;
  setTicketid(value: string): TicketComment;

  getUserid(): string;
  setUserid(value: string): TicketComment;

  getComment(): string;
  setComment(value: string): TicketComment;

  getTimestamp(): number;
  setTimestamp(value: number): TicketComment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketComment.AsObject;
  static toObject(includeInstance: boolean, msg: TicketComment): TicketComment.AsObject;
  static serializeBinaryToWriter(message: TicketComment, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketComment;
  static deserializeBinaryFromReader(message: TicketComment, reader: jspb.BinaryReader): TicketComment;
}

export namespace TicketComment {
  export type AsObject = {
    id: string,
    ticketid: string,
    userid: string,
    comment: string,
    timestamp: number,
  }
}

export class TicketWatcher extends jspb.Message {
  getTicketid(): string;
  setTicketid(value: string): TicketWatcher;

  getUserid(): string;
  setUserid(value: string): TicketWatcher;

  getRole(): Role;
  setRole(value: Role): TicketWatcher;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketWatcher.AsObject;
  static toObject(includeInstance: boolean, msg: TicketWatcher): TicketWatcher.AsObject;
  static serializeBinaryToWriter(message: TicketWatcher, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketWatcher;
  static deserializeBinaryFromReader(message: TicketWatcher, reader: jspb.BinaryReader): TicketWatcher;
}

export namespace TicketWatcher {
  export type AsObject = {
    ticketid: string,
    userid: string,
    role: Role,
  }
}

export class TicketRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): TicketRequest;
  hasHdr(): boolean;
  clearHdr(): TicketRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): TicketRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TicketRequest;
  hasPagination(): boolean;
  clearPagination(): TicketRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): TicketRequest;
  hasFilter(): boolean;
  clearFilter(): TicketRequest;

  getTicket(): Ticket | undefined;
  setTicket(value?: Ticket): TicketRequest;
  hasTicket(): boolean;
  clearTicket(): TicketRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TicketRequest): TicketRequest.AsObject;
  static serializeBinaryToWriter(message: TicketRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketRequest;
  static deserializeBinaryFromReader(message: TicketRequest, reader: jspb.BinaryReader): TicketRequest;
}

export namespace TicketRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    ticket?: Ticket.AsObject,
  }
}

export class TicketResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): TicketResponse;
  hasHdr(): boolean;
  clearHdr(): TicketResponse;

  getError(): string;
  setError(value: string): TicketResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TicketResponse;
  hasPagination(): boolean;
  clearPagination(): TicketResponse;

  getTicketsList(): Array<Ticket>;
  setTicketsList(value: Array<Ticket>): TicketResponse;
  clearTicketsList(): TicketResponse;
  addTickets(value?: Ticket, index?: number): Ticket;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TicketResponse): TicketResponse.AsObject;
  static serializeBinaryToWriter(message: TicketResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketResponse;
  static deserializeBinaryFromReader(message: TicketResponse, reader: jspb.BinaryReader): TicketResponse;
}

export namespace TicketResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    ticketsList: Array<Ticket.AsObject>,
  }
}

export class TicketCommentRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): TicketCommentRequest;
  hasHdr(): boolean;
  clearHdr(): TicketCommentRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): TicketCommentRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TicketCommentRequest;
  hasPagination(): boolean;
  clearPagination(): TicketCommentRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): TicketCommentRequest;
  hasFilter(): boolean;
  clearFilter(): TicketCommentRequest;

  getTicketcomment(): TicketComment | undefined;
  setTicketcomment(value?: TicketComment): TicketCommentRequest;
  hasTicketcomment(): boolean;
  clearTicketcomment(): TicketCommentRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketCommentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TicketCommentRequest): TicketCommentRequest.AsObject;
  static serializeBinaryToWriter(message: TicketCommentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketCommentRequest;
  static deserializeBinaryFromReader(message: TicketCommentRequest, reader: jspb.BinaryReader): TicketCommentRequest;
}

export namespace TicketCommentRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    ticketcomment?: TicketComment.AsObject,
  }
}

export class TicketCommentResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): TicketCommentResponse;
  hasHdr(): boolean;
  clearHdr(): TicketCommentResponse;

  getError(): string;
  setError(value: string): TicketCommentResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TicketCommentResponse;
  hasPagination(): boolean;
  clearPagination(): TicketCommentResponse;

  getTicketcommentsList(): Array<TicketComment>;
  setTicketcommentsList(value: Array<TicketComment>): TicketCommentResponse;
  clearTicketcommentsList(): TicketCommentResponse;
  addTicketcomments(value?: TicketComment, index?: number): TicketComment;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketCommentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TicketCommentResponse): TicketCommentResponse.AsObject;
  static serializeBinaryToWriter(message: TicketCommentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketCommentResponse;
  static deserializeBinaryFromReader(message: TicketCommentResponse, reader: jspb.BinaryReader): TicketCommentResponse;
}

export namespace TicketCommentResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    ticketcommentsList: Array<TicketComment.AsObject>,
  }
}

export class TicketWatcherRequest extends jspb.Message {
  getHdr(): common_pb.RequestHeader | undefined;
  setHdr(value?: common_pb.RequestHeader): TicketWatcherRequest;
  hasHdr(): boolean;
  clearHdr(): TicketWatcherRequest;

  getMethod(): common_pb.ModelCRUD;
  setMethod(value: common_pb.ModelCRUD): TicketWatcherRequest;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TicketWatcherRequest;
  hasPagination(): boolean;
  clearPagination(): TicketWatcherRequest;

  getFilter(): common_pb.Filter | undefined;
  setFilter(value?: common_pb.Filter): TicketWatcherRequest;
  hasFilter(): boolean;
  clearFilter(): TicketWatcherRequest;

  getTicketwatcher(): TicketWatcher | undefined;
  setTicketwatcher(value?: TicketWatcher): TicketWatcherRequest;
  hasTicketwatcher(): boolean;
  clearTicketwatcher(): TicketWatcherRequest;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketWatcherRequest.AsObject;
  static toObject(includeInstance: boolean, msg: TicketWatcherRequest): TicketWatcherRequest.AsObject;
  static serializeBinaryToWriter(message: TicketWatcherRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketWatcherRequest;
  static deserializeBinaryFromReader(message: TicketWatcherRequest, reader: jspb.BinaryReader): TicketWatcherRequest;
}

export namespace TicketWatcherRequest {
  export type AsObject = {
    hdr?: common_pb.RequestHeader.AsObject,
    method: common_pb.ModelCRUD,
    pagination?: common_pb.Pagination.AsObject,
    filter?: common_pb.Filter.AsObject,
    ticketwatcher?: TicketWatcher.AsObject,
  }
}

export class TicketWatcherResponse extends jspb.Message {
  getHdr(): common_pb.ResponseHeader | undefined;
  setHdr(value?: common_pb.ResponseHeader): TicketWatcherResponse;
  hasHdr(): boolean;
  clearHdr(): TicketWatcherResponse;

  getError(): string;
  setError(value: string): TicketWatcherResponse;

  getPagination(): common_pb.Pagination | undefined;
  setPagination(value?: common_pb.Pagination): TicketWatcherResponse;
  hasPagination(): boolean;
  clearPagination(): TicketWatcherResponse;

  getTicketwatchersList(): Array<TicketWatcher>;
  setTicketwatchersList(value: Array<TicketWatcher>): TicketWatcherResponse;
  clearTicketwatchersList(): TicketWatcherResponse;
  addTicketwatchers(value?: TicketWatcher, index?: number): TicketWatcher;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TicketWatcherResponse.AsObject;
  static toObject(includeInstance: boolean, msg: TicketWatcherResponse): TicketWatcherResponse.AsObject;
  static serializeBinaryToWriter(message: TicketWatcherResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TicketWatcherResponse;
  static deserializeBinaryFromReader(message: TicketWatcherResponse, reader: jspb.BinaryReader): TicketWatcherResponse;
}

export namespace TicketWatcherResponse {
  export type AsObject = {
    hdr?: common_pb.ResponseHeader.AsObject,
    error: string,
    pagination?: common_pb.Pagination.AsObject,
    ticketwatchersList: Array<TicketWatcher.AsObject>,
  }
}

export enum Status { 
  NULL = 0,
  NEW = 1,
  READ = 2,
  ASSIGNED = 3,
  INPROGRESS = 4,
  RESOLVED = 5,
}
export enum Type { 
  SUPPORT = 0,
  SALES = 1,
}
export enum Role { 
  WATCHER = 0,
  DECIDER = 1,
  INFORMER = 2,
}
