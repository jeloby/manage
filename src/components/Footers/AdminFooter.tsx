import { Row, Col, Nav, NavItem, NavLink } from "reactstrap";
import { AdminFooter } from "./FooterLinks";

const Footer = () => {
	return (
		<footer className='footer'>
			<Row className='align-items-center justify-content-xl-between'>
				<Col xl='6'>
					<div className='copyright text-center text-xl-left text-muted'>
						© {new Date().getFullYear()}{" "}
						<a
							className='font-weight-bold ml-1'
							href={AdminFooter.copyright.link}
							rel='noopener noreferrer'
							target='_blank'
						>
							{AdminFooter.copyright.text}
						</a>
					</div>
				</Col>
				<Col xl='6'>
					<Nav className='nav-footer justify-content-center justify-content-xl-end'>
						<NavItem>
							<NavLink href={AdminFooter.aboutUs.link} target='_blank'>
								{AdminFooter.aboutUs.text}
							</NavLink>
						</NavItem>

						<NavItem>
							<NavLink href={AdminFooter.blog.link} target='_blank'>
								{AdminFooter.blog.text}
							</NavLink>
						</NavItem>

						<NavItem>
							<NavLink href={AdminFooter.terms.link} target='_blank'>
								{AdminFooter.terms.text}
							</NavLink>
						</NavItem>

						<NavItem>
							<NavLink href={AdminFooter.softwareVersion.link} target='_blank'>
								V {AdminFooter.softwareVersion.text}
							</NavLink>
						</NavItem>
					</Nav>
				</Col>
			</Row>
		</footer>
	);
};

export default Footer;
