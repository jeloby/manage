const softwareVersion = {
	link: "",
	text: "0.0.1"
};
export const AuthFooter = {
	copyright: {
		link: "#",
		text: "Jeloby Systems"
	},
	aboutUs: {
		link: "#",
		text: "About Us"
	},
	blog: {
		link: "#",
		text: "Blog"
	},
	terms: {
		link: "#",
		text: "Terms & Conditions"
	},
	softwareVersion
};

export const AdminFooter = {
	copyright: {
		link: "#",
		text: "Jeloby Systems"
	},
	aboutUs: {
		link: "#",
		text: "About Us"
	},
	blog: {
		link: "#",
		text: "Blog"
	},
	terms: {
		link: "#",
		text: "Terms & Conditions"
	},
	softwareVersion
};
