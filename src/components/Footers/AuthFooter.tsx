import { NavItem, NavLink, Nav, Container, Row, Col } from "reactstrap";
import { AuthFooter } from "./FooterLinks";

const Login = () => {
	return (
		<>
			<footer className='py-5'>
				<Container>
					<Row className='align-items-center justify-content-xl-between'>
						<Col xl='6'>
							<div className='copyright text-center text-xl-left text-muted'>
								© {new Date().getFullYear()}{" "}
								<a
									className='font-weight-bold ml-1'
									href={AuthFooter.copyright.link}
									target='_blank'
									rel='noreferrer'
								>
									{AuthFooter.copyright.text}
								</a>
							</div>
						</Col>
						<Col xl='6'>
							<Nav className='nav-footer justify-content-center justify-content-xl-end'>
								<NavItem>
									<NavLink href={AuthFooter.aboutUs.link} target='_blank'>
										{AuthFooter.aboutUs.text}
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href={AuthFooter.blog.link} target='_blank'>
										{AuthFooter.blog.text}
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink href={AuthFooter.terms.link} target='_blank'>
										{AuthFooter.terms.text}
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink
										href={AuthFooter.softwareVersion.link}
										target='_blank'
									>
										V {AuthFooter.softwareVersion.text}
									</NavLink>
								</NavItem>
							</Nav>
						</Col>
					</Row>
				</Container>
			</footer>
		</>
	);
};

export default Login;
