export const GrpcServer: string = 'http://0.0.0.0:12000'
export const ServerError: string = 'error occured while communicating to server, please try again'
export const SessionToken: string = 'j_token'
export const SessionKey: string = 'j_key'
export type Method = 'GET' | 'DELETE' | 'POST' | 'PUT' | 'PATCH'

export type APIError = {
  message: string;
};

export interface Pagination {
  page?: number
  size?: number
  total?: number
}

export interface APIResponse {
  data: any[]
  pagination: Pagination
  error: string
}