import { UserServiceClient } from '../grpc/UserServiceClientPb';
import { UserRequest, UserResponse, User } from '../grpc/user_pb';
import { setPagination, setFilter, setMeta } from './helpers';
import { GrpcServer, ServerError } from "./types"

const client = new UserServiceClient(GrpcServer, null, null);
const errorResponse: UserResponse.AsObject = { error: ServerError, usersList: [] }

export const SelectAllUsers = async (search: string, page: number, size: number): Promise<UserResponse.AsObject> => {
  const request = new UserRequest()
  request.setFilter(setFilter(search))
  request.setPagination(setPagination(size, page));

  try {
    const resp = await client.selectAll(request, setMeta())
    return resp.toObject()
  } catch (error) {
    console.error(error)
    return errorResponse
  }
}

export const SelectUser = async (search: string): Promise<UserResponse.AsObject> => {
  const request = new UserRequest().setFilter(setFilter(search));

  try {
    const resp = await client.select(request, setMeta())
    return resp.toObject()
  } catch (error) {
    console.error(error)
    return errorResponse
  }
};

export const CreateUser = async (user: User): Promise<UserResponse.AsObject> => {
  const request = new UserRequest().setUser(user);

  try {
    const resp = await client.create(request, setMeta())
    return resp.toObject()
  } catch (error) {
    console.error(error)
    return errorResponse
  }
};

export const UpdateUser = async (user: User): Promise<UserResponse.AsObject> => {
  const request = new UserRequest().setUser(user);

  try {
    const resp = await client.update(request, setMeta())
    return resp.toObject()
  } catch (error) {
    console.error(error)
    return errorResponse
  }
};

export const DeleteUser = async (user_id: string): Promise<UserResponse.AsObject> => {
  const user = new User().setId(user_id);
  const request = new UserRequest().setUser(user);

  try {
    const resp = await client.delete(request, setMeta())
    return resp.toObject()
  } catch (error) {
    console.error(error)
    return errorResponse
  }
};
