import axios from 'axios';
import { Method, APIError, Pagination, APIResponse, GrpcServer } from './types';

import { UserServiceClient } from '../grpc/UserServiceClientPb';
import { UserRequest, UserResponse } from '../grpc/user_pb';
import { Filter } from '../grpc/common_pb';

export type { Method, APIError, Pagination, APIResponse };
// const { HelloRequest, HelloReply } = require('../rpc/helloworld_pb.js');
// const { GreeterClient } = require('../rpc/helloworld_grpc_web_pb');

// const baseURL: string = process.env.BASEURL!
const baseURL: string = 'https://dev.jeloby.com:8000';
const serverError: string = 'error occured while communicating to server, please try again'

const errorResponse = (errorMsg: string): APIResponse => ({
    data: [],
    pagination: { page: 0, size: 0, total: 0 },
    error: errorMsg,
});

const formatParams = (data: object): string => {
    let query: string = '';
    Object.entries(data).forEach(([key, value], index) => {
        query += index === 0 ? '' : '&';
        query += typeof value === 'string' ? `${key}='${value}'` : `${key}=${value}`;
    });

    return query;
};

export const apiCall = async (
    method: Method,
    url: string,
    pagination?: Pagination,
    query?: any,
    data?: any,
): Promise<APIResponse> => {
    try {
        let queryString: string = '';
        queryString += pagination ? formatParams(pagination) : '';
        queryString += query ? `&filter=${formatParams(query)}` : '';

        const newUrl = `${baseURL}${url}${queryString ? `?${queryString}` : ''}`;

        const resp = await axios({ method, url: newUrl, data });
        if (resp.status !== 200) {
            return errorResponse('received error response from server');
        }

        if (resp.data.status !== 'ok') {
            return errorResponse('received error response from unsplash server');
        }

        return { data: resp.data.data, pagination: resp.data.pagination, error: '' };
    } catch (error) {
        return errorResponse(`error receiving data from server: ${error}`);
    }
};

export const grpcCall = async (): Promise<UserResponse> => {
    const errorResponse = new UserResponse().setError(serverError)

    try {
        const client = new UserServiceClient(GrpcServer);
        const filter = new Filter().setRaw('company_id=12345');
        const request = new UserRequest().setFilter(filter);

        client.selectAll(request, {}).then((resp: UserResponse) => {
            console.log(resp.getUsersList);
            return resp
        });
        return errorResponse
    } catch (error) {
        console.error("")

        return errorResponse
    }
};
