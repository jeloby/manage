import { Filter, Pagination } from '../grpc/common_pb';
import { Metadata } from 'grpc-web';
import { SessionToken, SessionKey } from './types'

export const setPagination = (size: number, page: number): Pagination => {
  const pagination = new Pagination()
  pagination.setCurrent(page || 0)
  pagination.setSize(size || 10)

  return pagination
}

export const setFilter = (query: string): Filter => {
  const filter = new Filter();
  filter.setRaw(query)

  return filter
}

export const setMeta = (): Metadata => {
  return { jtoken: getToken(), jkey: getKey() }
  // return meta
}

export const setToken = (token: string) => {
  localStorage.setItem(SessionToken, token)
}

export const getToken = (): string => {
  const token = localStorage.getItem(SessionToken)
  return token || ""
}

export const setKey = (key: string) => {
  localStorage.setItem(SessionKey, key)
}

export const getKey = (): string => {
  const key = localStorage.getItem(SessionKey)
  return key || ""
}
