import sys
import os
import getopt
import time

DEVELOPMENT = "development"
STAGING = "staging"
PRODUCTION = "production"


def run(argv):
    environment = ""
    try:
        opts, args = getopt.getopt(argv, "he:o:", ["env="])
    except getopt.GetoptError:
        print("test.py -i <inputfile> -o <outputfile>")
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-h":
            print("test.py -i <inputfile> -o <outputfile>")
            sys.exit()
        elif opt in ("-e", "--env"):
            environment = arg

    if environment not in [DEVELOPMENT, STAGING, PRODUCTION]:
        print("Unknown environment: ", environment)

    remote_directory = "/var/www/staging.jeloby.com"

    if environment == DEVELOPMENT:
        remote_directory = "/var/www/development.jeloby.com"
        node = "node1"

    elif environment == STAGING:
        remote_directory = "/var/www/staging.jeloby.com"
        node = "node2"

    deploy(remote_directory, node)


def deploy(remote_directory, node):
    timestamp = int(time.time())
    file_name = "build.tar.bz2"

    os.system("yarn build --production")
    os.system(f"tar -cjvf {file_name} ./build")
    os.system(f"ssh {node} rm {remote_directory}/*.tar.*")
    os.system(f"ssh {node} rm -r {remote_directory}/current")
    os.system(f"scp {file_name} {node}:{remote_directory}")
    os.system(f"ssh {node} mkdir -p {remote_directory}/build/{timestamp}")
    extract_cmd = f"ssh {node} tar -xf {remote_directory}/{file_name} \
            -C {remote_directory}/build/{timestamp} \
            --strip-components=2"
    os.system(extract_cmd)
    link_cmd = f"ssh {node} ln -sf {remote_directory}/build/{timestamp} {remote_directory}/current"
    os.system(link_cmd)
    os.system("rm *.tar.bz2")


if __name__ == "__main__":
    try:
        run(sys.argv[1:])
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            # pylint: disable=W0212
            os._exit(0)
